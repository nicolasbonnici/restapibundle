<?php
namespace Nbo\RestApiBundle\Service;


use Nbo\RestApiBundle\Entity\AbstractResource;
use Nbo\RestApiBundle\Exception\ForbiddenException;
use Nbo\RestApiBundle\Helper\ApiHelper;
use Nbo\RestApiBundle\RestApiBundle;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * ACL layer
 *
 * Class AclService
 * @package Nbo\RestApiBundle\Service
 */
class AclService
{
    /** @var ApiHelper */
    protected $oApiHelper;

    /**
     * AclService constructor.
     * @param array $aConfig
     * @param ApiHelper $oApiHelper
     */
    public function __construct(ApiHelper $oApiHelper)
    {
        $this->oApiHelper = $oApiHelper;
    }

    /**
     * @param UserInterface|null $oUser
     * @param AbstractResource $oEntity
     * @param string $sMethod
     * @return bool
     */
    public function check(?UserInterface $oUser, AbstractResource $oEntity, string $sMethod): bool
    {
        $sResource  = $oEntity->resource();
        // Check for requested HTTP method
        $this->checkRequestedMethod($sResource, $sMethod);

        // Check for authentication layer if needed
        $this->checkAuthenticatedUser($oUser, $sResource, $sMethod);

        // Owner scope restricted option
        $this->checkOwnerRestrictedResource($oUser, $oEntity, $sMethod);

        return true;
    }

    /**
     * @param UserInterface $oUser
     * @param string $sResource
     * @param string $sMethod
     * @return bool
     */
    public function hasAccess(UserInterface $oUser, string $sResource, string $sMethod): bool
    {
        $bHasAccess  = false;
        foreach ($this->oApiHelper->getResourceMethodOption($sResource, $sMethod, RestApiBundle::ROLES) as $sRole) {
            if (
                in_array($sRole, $oUser->getRoles()) === true ||
                in_array(RestApiBundle::ROLE_ADMIN, $oUser->getRoles()) === true
            ) {
                $bHasAccess = true;
            }
        }
        return $bHasAccess;
    }

    /**
     * Is requested HTTP method allowed for the given resource
     *
     * @param string $sResource
     * @param string $sMethod
     */
    protected function checkRequestedMethod(string $sResource, string $sMethod)
    {
        if ($this->oApiHelper->hasMethodSupport($sResource, $sMethod) === false) {
            throw new ForbiddenException('Method not allowed.');
        }
    }

    /**
     * @param UserInterface|null $oUser
     * @param string $sResource
     * @param string $sMethod
     */
    protected function checkAuthenticatedUser(?UserInterface $oUser, string $sResource, string $sMethod)
    {
        if ($this->oApiHelper->getResourceMethodOption($sResource, $sMethod, RestApiBundle::AUTHENTICATED) === true ) {
            // Endpoint require authentication but no authenticated user found
            if (is_null($oUser) === true) {
                throw new UnauthorizedHttpException('', 'Not authenticated.');
            }

            // ACL check
            $aRoles = $this->oApiHelper->getResourceMethodOption($sResource, $sMethod, RestApiBundle::ROLES);
            if (is_array($aRoles) === true) {
                if ($this->hasAccess($oUser, $sResource, $sMethod) === false) {
                    throw new UnauthorizedHttpException('', 'Access control layer: Forbidden check your permissions.');
                }
            }
        }
    }

    protected function checkOwnerRestrictedResource(?UserInterface $oUser, AbstractResource $oEntity, $sMethod)
    {
        $sUserSetter = AbstractResource::DEFAULT_USER_SETTER;
        $sUserGetter = AbstractResource::DEFAULT_USER_GETTER;
        if (
            method_exists($oEntity, $sUserSetter) === true &&
            $this->oApiHelper->getResourceMethodOption($oEntity->resource(), $sMethod, RestApiBundle::OWNER_RESTRICTED)
        ) {
            // If user relation empty populate with authenticated user
            if (! $oEntity->{$sUserGetter}() && is_null($oUser) === false) {
                $oEntity->{$sUserSetter}($oUser);
            }

            // User scope restricted
            if (
                $oUser->getId() !== $oEntity->{$sUserGetter}()->getId() &&
                in_array(RestApiBundle::ROLE_ADMIN, $oUser->getRoles()) === false
            ) {
                throw new UnauthorizedHttpException('', 'Access control layer: Check your permissions, resource owned by an another user.');
            }
        }
    }

}

<?php

namespace Nbo\RestApiBundle\Service;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Gedmo\Translatable\Query\TreeWalker\TranslationWalker;
use Gedmo\Translatable\TranslatableListener;
use Nbo\RestApiBundle\Annotations\SubResource;
use Nbo\RestApiBundle\Entity\AbstractResource;
use Nbo\RestApiBundle\Filters\AbstractFilter;
use Nbo\RestApiBundle\Filters\EqualFilter;
use Nbo\RestApiBundle\Filters\IsNullFilter;
use Nbo\RestApiBundle\Helper\ApiHelper;
use Nbo\RestApiBundle\RestApiBundle;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Generic CRUD service
 *
 * Class CrudService
 * @package Nbo\RestApiBundle\Service
 */
class CrudService
{
    const ENTITY_ALIAS = 'e';

    /** @var EntityManager */
    private $entityManager;

    /** @var AclService */
    private $aclService;

    /** @var ApiHelper */
    private $apiHelper;

    /**
     * CrudService constructor.
     * @param EntityManager $oEntityManager
     * @param AclService $oAclService
     * @param ApiHelper $oApiHelper
     */
    public function __construct(
        EntityManager $oEntityManager,
        AclService $oAclService,
        ApiHelper $oApiHelper
    )
    {
        $this->entityManager = $oEntityManager;
        $this->aclService = $oAclService;
        $this->apiHelper = $oApiHelper;
    }

    /**
     * @param string $sClassName
     * @param array|null $aParameters
     * @param string|null $sLocale
     * @param array|null $aOrderBy
     * @param int $iPage
     * @param int|null $iLimit
     * @param int|null $iOffset
     * @return object|object[]|null
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function read(
        string $sClassName,
        ?array $aParameters = [],
        ?string $sLocale = 'en',
        ?array $aOrderBy = RestApiBundle::DEFAULT_ORDER,
        ?int $iPage = RestApiBundle::DEFAULT_PAGE,
        ?int $iLimit = RestApiBundle::DEFAULT_LIMIT,
        ?int $iOffset = RestApiBundle::DEFAULT_OFFSET
    )
    {
        return isset($aParameters[AbstractResource::DEFAULT_INTERNAL_PRIMARY_KEY_NAME]) === true
            ? $this->getById($sClassName, $aParameters[AbstractResource::DEFAULT_PRIMARY_KEY_NAME])
            : $this->list($sClassName, $aParameters, $sLocale, $aOrderBy, $iPage, $iLimit, $iOffset);
    }

    /**
     * @param AbstractResource $oEntity
     * @return AbstractResource|null
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function create(AbstractResource $oEntity): ?AbstractResource
    {
        $this->entityManager->persist($oEntity);
        $this->entityManager->flush();
        $this->entityManager->refresh($oEntity);

        return $oEntity;
    }

    /**
     * @param AbstractResource $oEntity
     * @return AbstractResource
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function update(AbstractResource $oEntity): AbstractResource
    {
        if (is_null($oEntity->getId()) === true) {
            throw new \InvalidArgumentException('No primary key identifier  parameters found on request.');
        }

        $this->entityManager->flush();
        $this->entityManager->refresh($oEntity);

        return $oEntity;
    }

    /**
     * @param AbstractResource $oEntity
     * @return AbstractResource
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(AbstractResource $oEntity): AbstractResource
    {
        $this->entityManager->remove($oEntity);
        $this->entityManager->flush();
        return $oEntity;
    }

    /**
     * Get Query's array output
     *
     * @param string $sClassName
     * @param array $aParameters
     * @param string $sLocale
     * @param array|null $aOrder
     * @param int|null $iPage
     * @param int|null $iLimit
     * @param int|null $iOffset
     * @return array
     */
    public function list(
        string $sClassName,
        array $aParameters,
        string $sLocale,
        ?array $aOrder = RestApiBundle::DEFAULT_ORDER,
        ?int $iPage = RestApiBundle::DEFAULT_PAGE,
        ?int $iLimit = RestApiBundle::DEFAULT_LIMIT,
        ?int $iOffset = RestApiBundle::DEFAULT_OFFSET
    ): array
    {
        return $this->getQueryResults(
            $this->buildQuery($sClassName, $aParameters, $sLocale, $aOrder, $iPage, $iLimit, $iOffset)
        );
    }

    /**
     * Get query results count
     *
     * @param string $sClassName
     * @param array $aParameters
     * @param string $sLocale
     * @param array|null $aOrder
     * @param int|null $iPage
     * @param int|null $iLimit
     * @param int|null $iOffset
     * @return int|null
     */
    public function count(
        string $sClassName,
        array $aParameters,
        string $sLocale,
        ?array $aOrder = RestApiBundle::DEFAULT_ORDER,
        ?int $iPage = RestApiBundle::DEFAULT_PAGE,
        ?int $iLimit = RestApiBundle::DEFAULT_LIMIT,
        ?int $iOffset = RestApiBundle::DEFAULT_OFFSET
    ): ?int
    {
        try {
            $oQuery = $this->buildQuery($sClassName, $aParameters, $sLocale, $aOrder, $iPage, $iLimit, $iOffset);
            $oQuery->select('count('. self::ENTITY_ALIAS . '.id' . ')');
            return $this->getQueryResults(
                $oQuery,
                AbstractQuery::HYDRATE_SINGLE_SCALAR
            );
        } catch (\Exception $oException) {
            return 0;
        }
    }

    /**
     * Generic QueryBuilder builder method
     *
     * @param string $sClassName
     * @param array $aParameters
     * @param string $sLocale
     * @param array|null $aOrder
     * @param int|null $iPage
     * @param int|null $iLimit
     * @param int|null $iOffset
     * @return QueryBuilder
     */
    protected function buildQuery(
        string $sClassName,
        array $aParameters,
        string $sLocale,
        ?array $aOrder = RestApiBundle::DEFAULT_ORDER,
        ?int $iPage = RestApiBundle::DEFAULT_PAGE,
        ?int $iLimit = RestApiBundle::DEFAULT_LIMIT,
        ?int $iOffset = RestApiBundle::DEFAULT_OFFSET
    ): QueryBuilder
    {
        if (is_int($iPage) === true && $iPage > 1) {
            $iOffset = ($iLimit * $iPage) - $iLimit;
        }

        /** @var QueryBuilder $oQueryBuilder */
        $oQueryBuilder = $this->entityManager->createQueryBuilder();

        $oQueryBuilder->select(self::ENTITY_ALIAS)
            ->from($sClassName, self::ENTITY_ALIAS);

        $this->buildQueryParameters($oQueryBuilder, $aParameters);

        // Build order and limit buildQuery clauses
        foreach ($aOrder as $sField => $sDirection) {
            $oQueryBuilder->addOrderBy(self::ENTITY_ALIAS . '.' . $sField, strtoupper($sDirection));
        }

        // Support translatable resources
        $oQueryBuilder->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class)
            ->setHint(TranslatableListener::HINT_TRANSLATABLE_LOCALE, $sLocale);

        $oQueryBuilder->setFirstResult($iOffset)
            ->setMaxResults($iLimit);

        return $oQueryBuilder;
    }

    /**
     * @param QueryBuilder $oQueryBuilder
     * @param string $sHydrationMode
     * @return mixed
     */
    public function getQueryResults(
        QueryBuilder $oQueryBuilder,
        string $sHydrationMode = AbstractQuery::HYDRATE_OBJECT
    )
    {
        return $oQueryBuilder->getQuery()
            ->useQueryCache(true)
            ->enableResultCache(RestApiBundle::DEFAULT_CACHE_TTL, $oQueryBuilder->getQuery()->getSQL()) // @todo dynamize cache TTL
            ->getResult($sHydrationMode);
    }

    /**
     * @param QueryBuilder $oQueryBuilder
     * @param array $aParameters
     * @return QueryBuilder
     */
    protected function buildQueryParameters(QueryBuilder $oQueryBuilder, array $aParameters): QueryBuilder
    {
        // First build buildQuery WHERE clauses with bounded parameters
        foreach ($aParameters as $sKey => $mValue) {
            $oFilter = null;
            if (is_array($mValue) === false) {
                if (is_null($mValue) === true) {
                    $oFilter = (new IsNullFilter($sKey, null));
                } else {
                    // Simple equal operator in WHERE clause
                    $oFilter = (new EqualFilter($sKey, $mValue, self::ENTITY_ALIAS));
                }
            } else {
                assert(count($mValue) === 1, 'Array must have two key first is the operator and second is the value');

                // Array value case OPERATOR => mValue example ['>' => 'foo'] see AbstractFilters
                $sOperator = array_key_first($mValue);
                $mValue = $mValue[$sOperator];

                if (array_key_exists($sOperator, AbstractFilter::MAPPING) === false) {
                    continue;
                }

                // Load filter
                $sFilterClass = AbstractFilter::MAPPING[$sOperator];
                $oFilter = new $sFilterClass($sKey, $mValue, self::ENTITY_ALIAS);
            }

            if (is_null($oFilter) === true) {
                continue;
            }

            // Add where condition directly from Filter's __toString method output
            if (strstr($oQueryBuilder->getQuery()->getSQL(), 'WHERE') === false) {
                $oQueryBuilder->where((string) $oFilter);
            } else {
                // @todo Query::orWhere() support needed too
                $oQueryBuilder->andWhere((string) $oFilter);
            }

            // Also add bounded parameter to query
            if (is_null($mValue) === false) {
                $oFilter->addQueryParameter($oQueryBuilder);
            }
        }
        return $oQueryBuilder;
    }

    /**
     * Generic entities populate method, handle scalar and relations setters and hydrate from an array
     *
     * @param AbstractResource $oEntity
     * @param array $aData
     * @param UserInterface|null $oUser
     * @return AbstractResource
     * @throws \Doctrine\ORM\ORMException
     */
    public function populate(AbstractResource $oEntity, array $aData, ?UserInterface $oUser = null): AbstractResource
    {
        foreach ($aData as $sKey => $mValue) {

            if (in_array($sKey, ['created', 'updated', 'deletedAt']) === true) {
                continue;
            }

            $sSetter = $oEntity->detectSetterMethodName($sKey);
            if (is_null($sSetter) === true) {
                continue;
            }
            $sParameterType = $oEntity->detectMethodParamType($sSetter);
            if (is_array($mValue) === false || $sParameterType === 'array') {
                $oEntity->{$sSetter}($mValue);
            } else {
                try {
                    $oAnnotation = $oEntity->detectPropertyAnnotations($sKey, SubResource::class);
                } catch(\Exception $oException) {
                    continue;
                }

                $sGetter = $oAnnotation->getter;
                $sRemoveMethod = $oAnnotation->remover;

                // Retrieve sub resource constraints
                $aConstraints = [];
                try {
                    $aConstraints = $this->buildConstraints($oEntity, $sKey, $oUser);
                } catch (\ReflectionException $e) {}

                if (empty($mValue) === true && $oEntity->{$sGetter}()->isEmpty() === false) {
                    // Empty sub resource relations
                    foreach ($oEntity->{$sGetter}() as $oSubResource) {
                        $oEntity->{$sRemoveMethod}($oSubResource);
                    }
                } else if (is_int(array_key_first($mValue)) === true) {
                    // Array of arrays case, mostly oneToMany and ManyToMany relations
                    foreach ($mValue as $aValue) {
                        $oRelatedEntity = $this->getOrMakeEntity($sParameterType, $aValue, $aConstraints);
                        // Add new relation
                        if ($oEntity->{$sGetter}()->contains($oRelatedEntity) === false) {
                            $oEntity->{$sSetter}($oRelatedEntity);
                        }
                    }

                    // Also handle relation deletion
                    foreach ($oEntity->{$sGetter}() as $oSubResource) {
                        if (is_null($oSubResource->getId()) === true) {
                            continue;
                        }

                        // Filter request values for sub resource if nothing found the sub resource is deleted
                        $aFilteredFromRequest = array_filter($mValue, function($aSubResource) use ($oSubResource) {
                            return isset($aSubResource[AbstractResource::DEFAULT_PRIMARY_KEY_NAME]) &&
                                    $aSubResource[AbstractResource::DEFAULT_PRIMARY_KEY_NAME] === $oSubResource->getId();
                        });
                        if (empty($aFilteredFromRequest) === true) {
                            $oEntity->{$sRemoveMethod}($oSubResource);
                        }
                    }
                } else if (is_string(array_key_first($mValue)) === true) {
                    // Normalized entity from OneToOne relation
                    $oRelatedEntity = $this->getOrMakeEntity($sParameterType, $mValue, $aConstraints);
                    $oEntity->{$sSetter}($oRelatedEntity);
                }
            }
        }

        return $oEntity;
    }

    /**
     * @param string $sClassName
     * @param int $iId
     * @return object|null
     */
    private function getById(string $sClassName, int $iId)
    {
        return $this->entityManager->getRepository($sClassName)->find($iId);
    }

    /**
     * @param AbstractResource $oResource
     * @param string $sAttribute
     * @param UserInterface|null $oUser
     * @return array
     * @throws \ReflectionException
     */
    public function buildConstraints(AbstractResource $oResource, string $sAttribute, ?UserInterface $oUser): array
    {
        $aOutput = [];
        $oSubResourceAnnotation = $oResource->detectPropertyAnnotations($sAttribute, SubResource::class);
        foreach ($oSubResourceAnnotation->constraints as $sConstraint => $sSource) {
            $aOutput[$sConstraint] = (
                $sSource === SubResource::SOURCE_PARENT
                    ? $oResource
                    : $oUser
            )->{'get' . ucfirst($sConstraint)}();
        }
        return $aOutput;
    }


    /**
     * @param string $sClassName
     * @param array $aData
     * @param array $aConstraints
     * @return AbstractResource
     * @throws \Doctrine\ORM\ORMException
     */
    private function getOrMakeEntity(string $sClassName, array $aData, array $aConstraints = []): AbstractResource
    {
        if (class_exists($sClassName) === false) {
            throw new NotFoundHttpException('Resource or class "' . $sClassName . '" not found.');
        }

        /** @var AbstractResource $oRelatedEntity */
        $oRelatedEntity = new $sClassName();

        // Add primary key to query constraints value if any provided
        if (isset($aData[AbstractResource::DEFAULT_PRIMARY_KEY_NAME]) === true) {
            $aConstraints[AbstractResource::DEFAULT_PRIMARY_KEY_NAME] = $aData[AbstractResource::DEFAULT_PRIMARY_KEY_NAME];
        }

        // Try to load related entity otherwise populate fresh instance
        try {
            $oEntity = $this->entityManager->getRepository($sClassName)->findOneBy($aConstraints);
            if (is_null($oEntity) === false) {
                return $oEntity;
            }
        } catch (\Exception $oException) {}

        $this->populate($oRelatedEntity, $aData);
        $this->entityManager->persist($oRelatedEntity);

        return $oRelatedEntity;
    }

}

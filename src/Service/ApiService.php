<?php

namespace Nbo\RestApiBundle\Service;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Nbo\RestApiBundle\Annotations\SubResource;
use Nbo\RestApiBundle\Entity\AbstractResource;
use Nbo\RestApiBundle\Event\ApiExceptionEvent;
use Nbo\RestApiBundle\Helper\ApiHelper;
use Nbo\RestApiBundle\RestApiBundle;
use Nbo\RestApiBundle\Serializer\ResourcesSerializer;
use Psr\Container\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * API service layer
 *
 * Class ApiService
 * @package Nbo\RestApiBundle\Service
 */
class ApiService
{
    const SUB_RESOURCE_TYPE_PREFIX = 'resource:';
    const SUB_RESOURCES_TYPE_PREFIX = 'resources:';

    /** @var ApiHelper */
    private $oApiHelper;

    /** @var AclService */
    private $oAcl;

    /**  @var CrudService */
    private $oCrud;

    /**  @var RouterInterface */
    private $oRouter;

    /**  @var EventDispatcher */
    private $oEventDispatcher;

    /**
     * ApiService constructor.
     * @param AclService $oAcl
     * @param CrudService $oCrud
     * @param ApiHelper $oApiHelper
     * @param RouterInterface $oRouter
     * @param EventDispatcherInterface $oEventDispatcher
     */
    public function __construct(
        AclService $oAcl,
        CrudService $oCrud,
        ApiHelper $oApiHelper,
        RouterInterface $oRouter,
        EventDispatcherInterface $oEventDispatcher
    )
    {
        $this->oAcl = $oAcl;
        $this->oCrud = $oCrud;
        $this->oApiHelper = $oApiHelper;
        $this->oRouter = $oRouter;
        $this->oEventDispatcher = $oEventDispatcher;
    }

    /**
     * @param UserInterface|null $oUser
     * @param string $sResourceName
     * @param Request $oRequest
     * @param int|null $iId
     * @param bool $bReturnCount
     * @return array|object|null
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function get(
        ?UserInterface $oUser,
        string $sResourceName,
        Request $oRequest,
        int $iId = null,
        bool $bReturnCount = false
    ){
        try {
            $oEntity = $this->loadEntity($sResourceName);

            $aParameters = [];
            $aOrderBy = RestApiBundle::DEFAULT_ORDER;
            $iPage = RestApiBundle::DEFAULT_PAGE;
            $iOffset = RestApiBundle::DEFAULT_OFFSET;
            $iLimit = RestApiBundle::DEFAULT_LIMIT;

            // Handle load by id
            if (is_null($iId) === false && $iId > 0) {
                $iId = filter_var($iId, FILTER_SANITIZE_NUMBER_INT);
                $aParameters[AbstractResource::DEFAULT_INTERNAL_PRIMARY_KEY_NAME] = $iId;
            }

            if ($oRequest->get('q')) {
                $aParameters = json_decode($oRequest->get('q'), true);
            }
            if ($oRequest->get('page')) {
                $iPage = filter_var($oRequest->get('page'), FILTER_SANITIZE_NUMBER_INT);
            }
            if ($oRequest->get('limit')) {
                $iLimit = filter_var($oRequest->get('limit'), FILTER_SANITIZE_NUMBER_INT);
            }
            if ($oRequest->get('offset')) {
                $iOffset = filter_var($oRequest->get('offset'), FILTER_SANITIZE_NUMBER_INT);
            }
            if ($oRequest->get('sort')) {
                $aOrderBy = $oRequest->get('sort');
            }

            // Check ACL layer
            $this->oAcl->check($oUser, $oEntity, __FUNCTION__);

            // Just count query results
            if ($bReturnCount === true) {
                return $this->oCrud->count(
                    get_class($oEntity),
                    $aParameters,
                    $oRequest->getLocale(),
                    $aOrderBy,
                    $iPage,
                    $iLimit,
                    $iOffset
                );
            }

            $oResponse = $this->oCrud->read(
                get_class($oEntity),
                $aParameters,
                $oRequest->getLocale(),
                $aOrderBy,
                $iPage,
                $iLimit,
                $iOffset
            );

            if (is_null($oResponse) === true) {
                throw new NotFoundHttpException('Resource not found.');
            }

            return $oResponse;
        } catch (\Exception $oException) {
            $this->oEventDispatcher->dispatch((new ApiExceptionEvent($oException)), ApiExceptionEvent::NAME);
            throw $oException;
        }
    }

    /**
     * @param UserInterface|null $oUser
     * @param string $sResourceName
     * @param Request $oRequest
     * @return object|array|null
     */
    public function post(?UserInterface $oUser, string $sResourceName, Request $oRequest): ?AbstractResource
    {
        try {
            $oEntity = $this->loadEntity($sResourceName);

            $aData = [];

            // Handle uploaded files and multipart/form-data submitted data
            $aUploadedfiles = $oRequest->files->all();
            if (empty($aUploadedfiles) === false) {
                $oEntity->files = $oRequest->files;
                $aReqData['data'] = $oRequest->request->all();
            } else {
                // Directly parse data from JSON request
                $aReqData = json_decode($oRequest->getContent(), true);
            }

            if (isset($aReqData['data']) === true) {
                $aData = $aReqData['data'];
            }

            // If  User is provided and resource entity has a foreign key to user resource
            if (is_null($oUser) === false && method_exists($oEntity, 'setUser' === true)) {
                $aData['user'] = $oUser->toArray();
            }

            $this->oCrud->populate($oEntity, $aData, $oUser);

            // Set entity locale
            if (method_exists($oEntity, 'setTranslatableLocale') === true) {
                $oEntity->setTranslatableLocale($oRequest->getLocale());
            }

            // Check ACL layer
            $this->oAcl->check($oUser, $oEntity, __FUNCTION__);

            return $this->oCrud->create($oEntity);
        } catch (\Exception $oException) {
            $this->oEventDispatcher->dispatch((new ApiExceptionEvent($oException)), ApiExceptionEvent::NAME);
            if ($oException instanceof UniqueConstraintViolationException) {
                throw new ConflictHttpException('Resource conflict.');
            }
            throw $oException;
        }
    }

    /**
     * @param UserInterface|null $oUser
     * @param string $sResourceName
     * @param Request $oRequest
     * @param int $iId
     * @return object|array|null
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function put(?UserInterface $oUser, string $sResourceName, Request $oRequest, int $iId)
    {
        try {
            $oEntity = $this->loadResourceEntity(
                $oRequest,
                $oUser,
                $this->getClassName($sResourceName),
                $iId
            );

            // Populate from request provided data
            $aReqData = json_decode($oRequest->getContent(), true);
            $this->oCrud->populate($oEntity, $aReqData['data'], $oUser);

            if (is_null($oEntity) === false) {
                return $this->oCrud->update($oEntity);
            }
            return null;
        } catch (\Exception $oException) {
            $this->oEventDispatcher->dispatch((new ApiExceptionEvent($oException)), ApiExceptionEvent::NAME);
            throw $oException;
        }
    }

    /**
     * @param UserInterface|null $oUser
     * @param string $sResourceName
     * @param Request $oRequest
     * @param int $iId
     * @return object|array|null
     */
    public function delete(?UserInterface $oUser, string $sResourceName, Request $oRequest, int $iId)
    {
        try {
            $oEntity = $this->loadResourceEntity(
                $oRequest,
                $oUser,
                $this->getClassName($sResourceName),
                $iId
            );

            if (is_null($oEntity) === false) {
                return $this->oCrud->delete($oEntity);
            }
            return $oEntity;
        } catch (\Exception $oException) {
            $this->oEventDispatcher->dispatch((new ApiExceptionEvent($oException)), ApiExceptionEvent::NAME);
            throw $oException;
        }
    }

    /**
     * @param mixed array|object $mResponse
     * @return int
     */
    public function getResponseStatusCode($mResponse): int
    {
        // Handle errors (serialized exceptions)
        if (is_array($mResponse) === true && isset($mResponse['error']) === true) {
            if (isset($mResponse['status']) === true) {
                return $mResponse['status'];
            }
            return 500;
        }
        return 200;
    }

    /**
     * Build API root (for client SDK)
     * @param ContainerInterface $oContainer
     * @param UserInterface|null $oUser
     * @param ResourcesSerializer $oSerializer
     * @return array
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function buildRoot(ContainerInterface $oContainer, ?UserInterface $oUser, ResourcesSerializer $oSerializer): array
    {
        // Collect bundle current configuration and add resource models infos for the SDK
        $aConfiguration = $oContainer->getParameter('rest_api');
        foreach ($aConfiguration[RestApiBundle::RESOURCES] as $sResource => $aResource) {

            if (isset($aResource['classname']) === false) {
                continue;
            }

            /** @var AbstractResource $oResource */
            $oResource = new $aResource['classname']();
            $oMetaData = $oContainer->get('doctrine')->getManager()->getClassMetadata($aResource['classname']);

            $aModel = [];
            // Iterate directly from normalized entity to keep the API fields scope
            foreach ($oSerializer->normalize($oResource, 'json', ['groups' => 'public']) as $sAttribute => $mValue) {
                if (isset($oMetaData->fieldMappings[$sAttribute]['type']) === true) {
                    $aModel[$sAttribute] = $oMetaData->getTypeOfField($sAttribute);
                } else {
                    // Detect and handle sub resource using SubResource annotation
                    $oSubResourceAnnotation = $oResource->detectPropertyAnnotations($sAttribute, SubResource::class);
                    if (is_null($oSubResourceAnnotation) === true) {
                        continue;
                    }
                    // Pluralize prefix type with "resource:" for 1 to 1 and many to 1 relations and "resources:" for others
                    $aModel[$sAttribute] = (
                            in_array($oSubResourceAnnotation->relation, ['OneToOne', 'ManyToOne']) === true
                                ? self::SUB_RESOURCE_TYPE_PREFIX
                                : self::SUB_RESOURCES_TYPE_PREFIX
                        ) . $oSubResourceAnnotation->name;
                }
            }

            $aConfiguration[RestApiBundle::RESOURCES][$sResource]['model'] = $aModel;
        }

        // Collect authenticated user infos
        $aUser = [];
        if ($oUser) {
            $aUser = $oSerializer->normalize($oUser, 'array', ['groups' => 'public']);
        }

        // Collect project's composer manifest
        $aComposerInfo = $this->getComposerManifest($oContainer);
        return [
            "version" => $aComposerInfo['require']['nbo/rest-api-bundle'] ?? 'Not installed.',
            "api_root" => $this->oRouter->generate('api_root', [], UrlGeneratorInterface::ABSOLUTE_URL),
            "authenticated_user" => $aUser,
            "configuration" => $aConfiguration,
            "composer" => $aComposerInfo
        ];
    }

    /**
     * @param Request $oRequest
     * @param UserInterface|null $oUser
     * @param string $sClassName
     * @param int $iId
     * @return AbstractResource|null
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function loadResourceEntity(Request $oRequest, ?UserInterface $oUser, string $sClassName, int $iId): ?AbstractResource
    {
        $aResults = $this->oCrud->read(
            $sClassName,
            [AbstractResource::DEFAULT_PRIMARY_KEY_NAME => $iId],
            $oRequest->getLocale()
        );

        if (empty($aResults) === true) {
            return null;
        }

        $oEntity = array_shift($aResults);

        // Check ACL layer
        $this->oAcl->check($oUser, $oEntity, strtolower($oRequest->getMethod()));

        return $oEntity;
    }

    /**
     * @param ContainerInterface $oContainer
     * @return array
     */
    protected function getComposerManifest(ContainerInterface $oContainer): array
    {
        $aComposerInfo = [];

        // Collect bundle infos
        try {
            $aComposerInfo = json_decode(
                file_get_contents(
                    $oContainer->getParameter('kernel.project_dir') . '/composer.json'
                ),
                true
            );
        } catch (\Exception $oException) {}

        return $aComposerInfo;
    }

    protected function getClassName($sResourceName)
    {
        $sClassName = $this->oApiHelper->getResourceOption($sResourceName, RestApiBundle::CLASSNAME);
        if (is_null($sClassName) === true) {
            // No mapping found for requested resource
            throw new NotFoundHttpException('Not found');
        }
        return $sClassName;
    }

    protected function loadEntity($sResourceName)
    {
        $sClassName = $this->oApiHelper->getResourceOption($sResourceName, RestApiBundle::CLASSNAME);
        if (class_exists($sClassName) === false) {
            throw new NotFoundHttpException('Resource not found.');
        }
        return new $sClassName();
    }
}

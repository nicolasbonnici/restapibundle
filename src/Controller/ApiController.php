<?php
namespace Nbo\RestApiBundle\Controller;

use Nbo\RestApiBundle\Serializer\ResourcesSerializer;
use Nbo\RestApiBundle\Service\ApiService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

/**
 * Generic API controller
 *
 * Class ApiController
 * @package Nbo\RestApiBundle\Controller
 */
class ApiController extends AbstractController
{
    /**
     * GET endpoint
     *
     * @Route("/{resource}", name="get", methods={"GET"})
     * @Route("/{resource}/{id}", name="getbyid", requirements={"id" = "\d+"}, defaults={"id" = null}, methods={"GET"})
     * @Route("/{_locale}/{resource}", name="getbylocale", methods={"GET"})
     * @Route("/{_locale}/{resource}/{id}", name="getbyidandlocale", requirements={"id" = "\d+"}, defaults={"id" = null}, methods={"GET"})
     *
     * @param string $resource
     * @param int|null $id
     * @param Request $oRequest
     * @param ApiService $oApiService
     * @param ResourcesSerializer $oSerializer
     * @return JsonResponse
     * @throws \Exception
     */
    public function getEndpoint(
        string $resource,
        ?int $id,
        Request $oRequest,
        ApiService $oApiService,
        ResourcesSerializer $oSerializer
    ): JsonResponse
    {
        $mResult = $oApiService->get(
            $this->getUser(),
            filter_var($resource, FILTER_SANITIZE_STRING),
            $oRequest,
            $id
        );
        $bHasError = isset($mResult['error']);
        $oResponse = JsonResponse::fromJSONString(
            $bHasError === false
                ? $oSerializer->serialize($mResult, 'json', [AbstractNormalizer::GROUPS => 'public'])
                : json_encode($mResult)
        );

        // Add resources total count in response headers if requested from request query string "count" flag
        if ($oRequest->get('count') && $bHasError === false) {
            $oResponse->headers->set(
                'Count',
                $oApiService->get(
                    $this->getUser(),
                    filter_var($resource, FILTER_SANITIZE_STRING),
                    $oRequest,
                    $id,
                    true
                )
            );
        }

        // Response status code
        $oResponse->setStatusCode(
            $oApiService->getResponseStatusCode($mResult)
        );

        return $oResponse;
    }

    /**
     * POST
     *
     * @Route("/{resource}", name="post", methods={"POST"})
     * @Route("/{_locale}/{resource}", name="postbylocale", methods={"POST"})
     *
     * @param string $resource
     * @param Request $oRequest
     * @param ApiService $oApiService
     * @param ResourcesSerializer $oSerializer
     * @return JsonResponse
     * @throws \Exception
     */
    public function postEndpoint(
        string $resource,
        Request $oRequest,
        ApiService $oApiService,
        ResourcesSerializer $oSerializer
    ): JsonResponse
    {
        $mResult = $oApiService->post($this->getUser(), $resource, $oRequest);
        return JsonResponse::fromJSONString(
            $oSerializer->serialize(
                $mResult,
                'json',
                [AbstractNormalizer::GROUPS => 'public']
            )
        )->setStatusCode(
            $oApiService->getResponseStatusCode($mResult)
        );
    }

    /**
     * PUT
     *
     * @Route("/{resource}/{id}", name="put", requirements={"id" = "(\d+)"}, methods={"PUT"})
     * @Route("/{_locale}/{resource}/{id}", name="putbylocale", requirements={"id" = "(\d+)"}, methods={"PUT"})
     *
     * @param string $resource
     * @param int $id
     * @param Request $oRequest
     * @param ApiService $oApiService
     * @param ResourcesSerializer $oSerializer
     * @return JsonResponse
     * @throws \Exception
     */
    public function putEndpoint(
        string $resource,
        int $id,
        Request $oRequest,
        ApiService $oApiService,
        ResourcesSerializer $oSerializer
    ): JsonResponse
    {
        $mResult = $oApiService->put($this->getUser(), $resource, $oRequest, $id);
        return JsonResponse::fromJSONString(
            $oSerializer->serialize(
                $mResult,
                'json',
                [AbstractNormalizer::GROUPS => 'public']
            )
        )->setStatusCode(
            $oApiService->getResponseStatusCode($mResult)
        );
    }

    /**
     * DELETE
     *
     * @Route("/{resource}/{id}", name="delete", requirements={"id" = "(\d+)"}, methods={"DELETE"})
     *
     * @param string $resource
     * @param int $id
     * @param Request $oRequest
     * @param ApiService $oApiService
     * @param ResourcesSerializer $oSerializer
     * @return JsonResponse
     * @throws \Exception
     */
    public function deleteEndpoint(
        string $resource,
        int $id,
        Request $oRequest,
        ApiService $oApiService,
        ResourcesSerializer $oSerializer
    ): JsonResponse
    {
        $mResult = $oApiService->delete($this->getUser(), $resource, $oRequest, $id);
        return JsonResponse::fromJSONString(
            $oSerializer->serialize(
                $mResult,
                'json',
                [AbstractNormalizer::GROUPS => 'public']
            )
        )->setStatusCode(
            $oApiService->getResponseStatusCode($mResult)
        );
    }

    /**
     * @Route("/", name="api_root", methods={"GET"}))
     *
     * @param Request $oRequest
     * @param ContainerInterface $oContainer
     * @param ResourcesSerializer $oSerializer
     * @return JsonResponse
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function root(Request $oRequest, ContainerInterface $oContainer, ResourcesSerializer $oSerializer, ApiService $oApiService)
    {
        return new JsonResponse(
            $oApiService->buildRoot($oContainer, $this->getUser(), $oSerializer)
        );
    }

}

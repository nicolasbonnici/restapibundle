<?php
namespace Nbo\RestApiBundle\Controller;

use Nbo\RestApiBundle\Serializer\ResourcesSerializer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 * @package Nbo\RestApiBundle\Controller
 */
class UserController extends AbstractController
{

    /**
     * @Route("/", name="api_user_index", methods={"GET"}))
     * @param ResourcesSerializer $oSerializer
     * @return JsonResponse
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function index(ResourcesSerializer $oSerializer)
    {
        // Collect authenticated user infos
        $aUser = $oSerializer->normalize($this->getUser(), 'array', ['groups' => 'public']);
        return JsonResponse::fromJsonString(
            $oSerializer->serialize($aUser, 'json', ['groups' => 'public'])
        );
    }

}

<?php


namespace Nbo\RestApiBundle\Annotations;

/**
 * Class SubResource
 * @package RestApiBundle\Annotations
 * @Annotation
 * @Target({"PROPERTY"})
 */
class SubResource {

    /**
     * Constraints source types
     */
    const SOURCE_PARENT = 'parent';
    const SOURCE_USER = 'user';

    /**
     * Sub resource name
     *
     * @var string
     * @Annotation\Required()
     */
    public $name;

    /**
     * Entity relation type (OneToOne, ManyToOne, OneToMany, ManyToMany)
     *
     * @var string
     * @Annotation\Required()
     */
    public $relation = 'OneToOne';

    /**
     * Foreign keys constraints to scope sub resources ['key' => 'parent || user']
     * @var array
     */
    public $constraints = [];

    /**
     * Getter method name
     * @Annotation\Required()
     * @var string
     */
    public $getter = null;

    /**
     * Setter method name
     * @Annotation\Required()
     * @var string
     */
    public $setter = null;

    /**
     * Remove method name
     * @var string
     */
    public $remover = null;

}

<?php


namespace Nbo\RestApiBundle\Annotations;

use Doctrine\Common\Annotations\Annotation;

/**
 * Class Resource
 * @package RestApiBundle\Annotations
 * @Annotation
 * @Target({"CLASS"})
 */
class Resource
{
    /**
     * @var string
     * @Annotation\Required()
     */
    public $name;

    /**
     * @var string
     */
    public $primaryKey = 'id';
}

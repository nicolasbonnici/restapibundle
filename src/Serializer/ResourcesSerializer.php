<?php
namespace Nbo\RestApiBundle\Serializer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Generic resources serializer, can handle resource entities and array of resource entities
 *
 * Class ResourcesSerializer
 * @package Nbo\RestApiBundle\Serializer
 */
class ResourcesSerializer implements SerializerInterface, NormalizerInterface
{
    const DEFAULT_SERIALIZATION_FORMAT = 'json';

    /** @var Serializer  */
    protected $oSerializer;

    public function __construct()
    {
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $this->oSerializer = new Serializer(
            [
                (new GetSetMethodNormalizer($classMetadataFactory, null, null, null, null, $this->getDefaultContext()))
            ],
            [
                (new JsonEncoder())
            ]
        );
    }

    /**
     * Serializes data in the appropriate format.
     *
     * @param mixed $data Any data
     * @param string $format Format name
     * @param array $context Options normalizers/encoders have access to
     *
     * @return string
     */
    public function serialize($data, $format, array $context = [])
    {
        $aCtx = array_merge($context, $this->getDefaultContext());
        return $this->oSerializer->serialize($data, $format, $aCtx);
    }

    /**
     * Deserializes data into the given type.
     *
     * @param mixed $data
     * @param string $type
     * @param string $format
     *
     * @param array $context
     * @return object|array
     */
    public function deserialize($data, $type, $format, array $context = [])
    {
        return $this->oSerializer->deserialize($data, $format, $context);
    }

    /**
     * Normalizes an object into a set of arrays/scalars.
     *
     * @param mixed $object Object to normalize
     * @param string $format Format the normalization result will be encoded as
     * @param array $context Context options for the normalizer
     *
     * @return array|string|int|float|bool
     *
     * @throws \Symfony\Component\Serializer\Exception\InvalidArgumentException   Occurs when the object given is not an attempted type for the normalizer
     * @throws \Symfony\Component\Serializer\Exception\CircularReferenceException Occurs when the normalizer detects a circular reference when no circular reference handler can fix it
     * @throws \Symfony\Component\Serializer\Exception\LogicException             Occurs when the normalizer is not called in an expected context
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface         Occurs for all the other cases of errors
     */
    public function normalize($object, $format = self::DEFAULT_SERIALIZATION_FORMAT, array $context = [])
    {
        try {
            $aCtx = array_merge($context, $this->getDefaultContext());
            return $this->oSerializer->normalize($object, $format, $aCtx);
        } catch (\Exception $oException) {
            return null;
        }
    }

    /**
     * Checks whether the given class is supported for normalization by this normalizer.
     *
     * @param mixed $data Data to normalize
     * @param string $format The format being (de-)serialized from or into
     *
     * @return bool
     */
    public function supportsNormalization($data, $format = self::DEFAULT_SERIALIZATION_FORMAT)
    {
        // Support for entities, collection and arrays of entities
        return (bool) (is_object($data) || is_array($data));
    }

    /**
     * @return array
     */
    private function getDefaultContext(): array
    {
        return [
            AbstractNormalizer::CALLBACKS => [
                'created' => $this->buildDatetimeCallbackFunction(),
                'updated' => $this->buildDatetimeCallbackFunction(),
                'deletedAt' => $this->buildDatetimeCallbackFunction()
            ],
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ];
    }

    /**
     *
     * @return \Closure
     */
    private function buildDatetimeCallbackFunction(): \Closure
    {
        return function ($innerObject, $outerObject, string $attributeName, string $format = null, array $context = []) {
            return $innerObject instanceof \DateTime ? $innerObject->format(\DateTime::ATOM) : '';
        };
    }

}

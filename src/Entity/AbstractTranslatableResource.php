<?php
namespace Nbo\RestApiBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class AbstractTranslatableResource
 * @package RestApiBundle\Entity
 */
abstract class AbstractTranslatableResource extends AbstractResource
{

    /**
     * Post locale
     * Used locale to override Translation listener's locale
     *
     * @Gedmo\Locale
     */
    protected $locale;

    /**
     * Sets translatable locale
     *
     * @param string $locale
     */
    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }
}

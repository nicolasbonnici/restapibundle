<?php
namespace Nbo\RestApiBundle\Entity;

use Doctrine\Common\Annotations\AnnotationReader;
use Nbo\RestApiBundle\Annotations\Resource;
use Nbo\RestApiBundle\Annotations\SubResource;
use Nbo\RestApiBundle\Helper\StdClassHelper;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Zend\Code\Annotation\AnnotationManager;
use Zend\Code\Reflection\MethodReflection;
use Zend\Code\Reflection\ParameterReflection;

/**
 * Class AbstractEntity
 * @package RestApiBundle\Entity
 */
abstract class AbstractResource
{
    const DEFAULT_PRIMARY_KEY_NAME = 'id';
    const DEFAULT_INTERNAL_PRIMARY_KEY_NAME = 'resource_id';
    const DEFAULT_USER_FOREIGN_KEY_NAME = 'user';
    const DEFAULT_USER_GETTER = 'getUser';
    const DEFAULT_USER_SETTER = 'setUser';

    const SETTER_PREFIXES = ['set', 'add'];

    /**
     * @var string
     */
    protected $primaryKeyName = self::DEFAULT_PRIMARY_KEY_NAME;

    /**
     * Retrieve resource name
     *
     * @return string
     * @throws \ReflectionException
     */
    public function resource(): string
    {
        $oAnnotationReader = new AnnotationReader();
        $oResourceAnnotation = $oAnnotationReader->getClassAnnotation(new \ReflectionClass($this), Resource::class);
        if ($oResourceAnnotation instanceof Resource) {
            return $oResourceAnnotation->name;
        }
        trigger_error('Resource annotation error, name key missing.', E_USER_NOTICE);
        return get_class($this);
    }

    /**
     * Get business object attributes
     *
     * @return array
     * @throws \ReflectionException
     */
    public function attributes(): array
    {
        $aOutput = [];
        $oReflected = new \ReflectionClass(get_class($this));
        foreach ($oReflected->getProperties() as $oProperty) {
            $aOutput[] = $oProperty->getName();
        }
        return $aOutput;
    }

    /**
     * @return array
     */
    public function detectGetters(): array
    {
        $aOutput = [];
        foreach (get_class_methods(get_class($this)) as $sMethod) {
            if (
                substr($sMethod, 0, strlen('get')) === 'get' &&
                in_array($sMethod, [__METHOD__, 'detectSetterMethodName', 'detectMethodParamType']) === false
            ) {
                $aOutput[] = $sMethod;
            }
        }
        return $aOutput;
    }

    /**
     * @param string $sAttribute
     * @return string|null
     */
    public function detectSetterMethodName(string $sAttribute): ?string
    {
        $oConvert = new CamelCaseToSnakeCaseNameConverter();
        foreach (self::SETTER_PREFIXES as $sPrefix) {
            $sSetter = $sPrefix . ucfirst($oConvert->denormalize($sAttribute));
            if (method_exists(get_class($this), $sSetter) === true) {
                return $sSetter;
            }

            // Test without pluralization
            $sSetter = substr($sSetter, 0, strlen($sSetter) - 1);
            if (method_exists(get_class($this), $sSetter) === true) {
                return $sSetter;
            }
            // Handle entity => entities case
            $sSetter = substr($sSetter, 0, strlen($sSetter) - strlen('ie')) . 'y';
            if (method_exists(get_class($this), $sSetter) === true) {
                return $sSetter;
            }
        }

        return null;
    }

    /**
     * @param string $sSetterMethodName
     * @return string|null
     */
    public function detectMethodParamType(string $sSetterMethodName): ?string
    {
        $sType = null;

        // Detect if it's a doctrine relation
        $oReflectedMethod = new MethodReflection(get_class($this), $sSetterMethodName);
        $aParameters = $oReflectedMethod->getParameters();
        if (isset($aParameters[0]) === true) {
            /** @var ParameterReflection $oReflectedParam */
            $oReflectedParam = $aParameters[0];
            if ($oReflectedParam->detectType()) {
                $sType = $oReflectedParam->detectType();
            }
        }
        return $sType;
    }

    /**
     * @param string $sAttribute
     * @param string $sAnnotationClassname
     * @return object|null
     * @throws \ReflectionException
     */
    public function detectPropertyAnnotations(string $sAttribute, string $sAnnotationClassname)
    {
        $oAnnotationReader = new AnnotationReader();
        return $oAnnotationReader->getPropertyAnnotation(
            new \ReflectionProperty($this, $sAttribute), $sAnnotationClassname
        );
    }

    /**
     * Cast Entity instance as an array
     *
     * @return array
     * @throws \ReflectionException
     */
    public function toArray(): array
    {
        $aOutput = [];
        // Use Reflection class to avoid weird output array keys for private and protected attributes
        // when trying to cast instance as an array
        $oReflected = new \ReflectionClass(get_class($this));
        foreach ($oReflected->getProperties() as $oProperty) {
            $oProperty->setAccessible(true);
            $aOutput[$oProperty->getName()] = $oProperty->getValue($this);
            $oProperty->setAccessible(false);
        }
        return $aOutput;
    }

    /**
     * Cast entity as an anonymous object
     *
     * @return \stdClass
     * @throws \ReflectionException
     */
    public function toObject(): \stdClass
    {
        $oOutput = new StdClassHelper();
        foreach ($this->toArray() as $sKey => $mValue) {
            $oOutput->{$sKey} = $mValue;
        }
        return $oOutput->toObject();
    }

    public function toJson(): string
    {
        return json_encode($this, JSON_PRETTY_PRINT);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return get_class($this) . '#' . $this->getId();
    }
}

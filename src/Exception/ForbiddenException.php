<?php
namespace Nbo\RestApiBundle\Exception;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ForbiddenException extends AccessDeniedHttpException {}

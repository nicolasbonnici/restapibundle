<?php
namespace Nbo\RestApiBundle\Helper;


use Nbo\RestApiBundle\RestApiBundle;

class ApiHelper
{
    /** @var array  */
    protected $aBundleConfig = [];

    public function __construct(array $aBundleConfig)
    {
        $this->aBundleConfig = $aBundleConfig;
    }

    /**
     * @param string $sConfigurationKey
     * @return int
     */
    public function getConfiguration(string $sConfigurationKey): int
    {
        if (isset($this->aBundleConfig[$sConfigurationKey]) === true) {
            return $this->aBundleConfig[$sConfigurationKey];
        }
        return null;
    }

    /**
     * @param string $sResourceName
     * @return array|null
     */
    public function getResource(string $sResourceName): ?array
    {
        if (isset($this->aBundleConfig[RestApiBundle::RESOURCES][$sResourceName]) === true) {
            return $this->aBundleConfig[RestApiBundle::RESOURCES][$sResourceName];
        }
        return null;
    }

    /**
     * @param string $sResourceName
     * @param string $sKey
     * @return mixed
     */
    public function getResourceOption(string $sResourceName, string $sKey)
    {
        $aResource = $this->getResource($sResourceName);
        if (is_array($aResource) === true && isset($aResource[$sKey]) === true) {
            return $aResource[$sKey];
        }
        return null;
    }

    /**
     * Get resource HTTP method options
     *
     * @param string $sResourceName
     * @param string $sMethodName
     * @return array|null
     */
    public function getResourceMethodOptions(string $sResourceName, string $sMethodName): ?array
    {
        $aResource = $this->getResource($sResourceName);
        if (is_array($aResource) === true) {
            if (isset($aResource[RestApiBundle::HTTP_METHODS][$sMethodName]) === true) {
                return $aResource[RestApiBundle::HTTP_METHODS][$sMethodName];
            }
        }
        return null;
    }

    /**
     * Tell if requested method is supported for given resource
     *
     * @param string $sResource
     * @param string $sMethod
     * @return bool
     */
    public function hasMethodSupport(string $sResource, string $sMethod): bool
    {
        $aOptions = $this->getResourceOption($sResource, RestApiBundle::HTTP_METHODS);
        return array_key_exists($sMethod, $aOptions) === true;
    }

    /**
     * Get resource HTTP method option
     *
     * @param string $sResourceName
     * @param string $sMethodName
     * @param string $sKey
     * @return mixed|null
     */
    public function getResourceMethodOption(string $sResourceName, string $sMethodName, string $sKey)
    {
        $aOptions = $this->getResourceMethodOptions($sResourceName, $sMethodName);
        if (is_array($aOptions) && isset($aOptions[$sKey]) === true) {
            return $aOptions[$sKey];
        }
        return null;
    }
}

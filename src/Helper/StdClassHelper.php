<?php
namespace Nbo\RestApiBundle\Helper;

/**
 * Class StdClassHelper simple \stdClass helper
 * @package Nbo\RestApiBundle\Helper
 */
class StdClassHelper
{
    /**
     * @var \stdClass
     */
    private $oData;

    /**
     * StdClassHelper constructor.
     * @param \stdClass|null $oObject
     */
    public function __construct(\stdClass $oObject = null)
    {
        $this->oData = new \stdClass();
        if (is_null($oObject) !== true) {
            $this->oData = $oObject;
        }
    }

    /**
     * @return false|string
     */
    public function __toString(): string
    {
        return $this->toJson();
    }

    /**
     * Generic getter
     *
     * @param string $sAttr
     * @return mixed|null
     */
    public function __get(string $sAttr)
    {
        if (isset($this->oData->{$sAttr}) === true) {
            return $this->oData->{$sAttr};
        }
        return null;
    }

    /**
     * Generic setter
     *
     * @param string $sAttr
     * @param mixed $mValue
     * @return \App\SocialMediaBundle\Helper\StdClassHelper
     */
    public function __set(string $sAttr, $mValue): StdClassHelper
    {
        $this->oData->{$sAttr} = $mValue;
        return $this;
    }

    /**
     * @param string $sKey
     * @return bool
     */
    public function hasKey(string $sKey): bool
    {
        return isset($this->oData->{$sKey});
    }

    /**
     * Convert instance data to array format
     *
     * @return array
     */
    public function toArray(): array
    {
        return get_object_vars($this->oData);
    }

    /**
     * @return \stdClass
     */
    public function toObject(): \stdClass
    {
        return $this->oData;
    }

    /**
     * Convert instance data to JSON representation
     *
     * @return false|string
     */
    public function toJson(): string
    {
        return json_encode($this->oData);
    }

}

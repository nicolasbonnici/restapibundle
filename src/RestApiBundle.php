<?php
namespace Nbo\RestApiBundle;

use Nbo\RestApiBundle\DependencyInjection\Compiler\RestApiCompilerPass;
use Nbo\RestApiBundle\DependencyInjection\RestApiExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class RestApiBundle extends Bundle {

    /**
     * Pagination defaults
     */
    const DEFAULT_PAGE = 1;
    const DEFAULT_LIMIT = 10;
    const DEFAULT_OFFSET = 0;
    const DEFAULT_ORDER_BY = 'updated';
    const DEFAULT_ORDER_DIRECTION = 'desc';
    const DEFAULT_CACHE_TTL = 3600; // ms
    const DEFAULT_ORDER = [self::DEFAULT_ORDER_BY => self::DEFAULT_ORDER_DIRECTION];

    /**
     * Configuration variable key names
     */
    const ROOT_NAME = 'api_rest';
    const ORDER = 'order';
    const LIMIT = 'limit';
    const OFFSET = 'offset';
    const RESOURCES = 'resources';
    const CLASSNAME = 'classname';
    const HTTP_METHODS = 'methods';
    const GET_METHOD = 'get';
    const POST_METHOD = 'post';
    const PUT_METHOD = 'put';
    const DELETE_METHOD = 'delete';
    const AUTHENTICATED = 'authenticated';
    const OWNER_RESTRICTED = 'owner_restricted';
    const ROLES = 'roles';

    /**
     * Parameters
     */
    const ROLE_ADMIN = 'ROLE_ADMIN';

    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        // Register bundle extension
        $container->registerExtension(new RestApiExtension());

        $container->addCompilerPass(new RestApiCompilerPass());
    }

}

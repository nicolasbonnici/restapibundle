<?php
namespace Nbo\RestApiBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class RestApiExtension extends Extension
{
    /**
     * @param array $configs
     * @param ContainerBuilder $container
     * @throws \Exception
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        // Load services configuration
        $oYamlLoader = new YamlFileLoader(
            $container,
            new FileLocator(dirname(__DIR__) . '/../config')
        );

        // Register bundle services
        $oYamlLoader->load('services.yaml');

        // Register bundle API enpoints routes
        $oYamlLoader->load('routes.yaml');

        $oRestApiBundleConfiguration = new Configuration();
        $this->processConfiguration($oRestApiBundleConfiguration, $configs);
        $container->setParameter('rest_api', $configs[0]);
        $container->setParameter('rest_api.root', $configs[0]);
        $container->setParameter('rest_api.default_limit', $configs[0]['default_limit']);
        $container->setParameter('rest_api.default_offset', $configs[0]['default_offset']);
        $container->setParameter('rest_api.default_order', $configs[0]['default_order']);
        $container->setParameter('rest_api.resources', $configs[0]['resources']);
    }

}

<?php
namespace Nbo\RestApiBundle\DependencyInjection;

use Nbo\RestApiBundle\RestApiBundle;
use Symfony\Component\Config\Definition\ArrayNode;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class ApiConfiguration
 * @package Nbo\RestApiBundle\Configuration
 */
class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('rest_api');

        $treeBuilder->getRootNode()
            ->children()
            ->scalarNode('default_limit')->defaultValue(RestApiBundle::DEFAULT_LIMIT)->end()
            ->scalarNode('default_offset')->defaultValue(RestApiBundle::DEFAULT_OFFSET)->end()
            ->scalarNode('default_cache_ttl')->defaultValue(RestApiBundle::DEFAULT_CACHE_TTL)->end()
            ->arrayNode('default_order')
                ->variablePrototype()->defaultValue(RestApiBundle::DEFAULT_ORDER)->end()
            ->end()
            ->arrayNode('resources')
                ->arrayPrototype()
                ->children()
                    ->scalarNode('classname')->end()
                    ->arrayNode('methods')->useAttributeAsKey('method')
                        ->arrayPrototype()
                        ->children()
                            ->scalarNode('authenticated')->end()
                            ->scalarNode('owner_restricted')->end()
                            ->arrayNode('roles')->scalarPrototype()->end()
                    ->end()
            ->end()
        ->end();

        return $treeBuilder;
    }

}

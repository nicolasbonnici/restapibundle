<?php
namespace Nbo\RestApiBundle\Event;

/**
 * Class ApiExceptionEvent
 *
 * This Event is dispatched at each API exception
 */
class ApiExceptionEvent extends \Symfony\Contracts\EventDispatcher\Event
{
    public const NAME = 'api.exception';

    /** @var \Exception */
    protected $oException = null;

    public function __construct(\Exception $oException)
    {
        $this->oException = $oException;
    }

    public function getException(): ?\Exception
    {
        return $this->oException;
    }
}

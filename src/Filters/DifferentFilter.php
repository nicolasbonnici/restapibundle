<?php
namespace Nbo\RestApiBundle\Filters;

/**
 * Class DifferentFilter
 * @package Nbo\RestApiBundle\Filters
 */
class DifferentFilter extends AbstractFilter {
    protected $sOperator = AbstractFilter::OPERATOR_DIFFERENT;
}

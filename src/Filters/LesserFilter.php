<?php
namespace Nbo\RestApiBundle\Filters;

/**
 * Class LesserFilter
 * @package Nbo\RestApiBundle\Filters
 */
class LesserFilter extends AbstractFilter {
    protected $sOperator = AbstractFilter::OPERATOR_LESSER;
}

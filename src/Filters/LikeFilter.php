<?php
namespace Nbo\RestApiBundle\Filters;

use Doctrine\ORM\QueryBuilder;

/**
 * Class LikeFilter
 * @package Nbo\RestApiBundle\Filters
 */
class LikeFilter extends AbstractFilter {
    const WILDCARD = '%';

    const DEFAULT_WILDCARD = 'full';
    const WILDCARD_START = 'start';
    const WILDCARD_END = 'end';

    protected $sOperator = AbstractFilter::OPERATOR_LIKE;

    public function addQueryParameter(QueryBuilder $oQuery, string $sWildcard = self::DEFAULT_WILDCARD): QueryBuilder
    {
        switch ($sWildcard){
            case (self::WILDCARD_START) :
                $oQuery->setParameter($this->getKey(), self::WILDCARD . $this->getValue());
                break;
            case self::WILDCARD_END :
                $oQuery->setParameter($this->getKey(), $this->getValue() . self::WILDCARD);
                break;
            default:
                $oQuery->setParameter($this->getKey(), self::WILDCARD . $this->getValue() . self::WILDCARD);
        }
        return $oQuery;
    }

}

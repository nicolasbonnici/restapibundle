<?php
namespace Nbo\RestApiBundle\Filters;

/**
 * Class IsNotNullFilter
 * @package Nbo\RestApiBundle\Filters
 */
class IsNotNullFilter extends AbstractFilter {
    protected $sOperator = AbstractFilter::OPERATOR_IS_NOT_NULL;

    protected function build(): string
    {
        return $this->getPrefixedKey() . ' ' . $this->sOperator;
    }


}

<?php
namespace Nbo\RestApiBundle\Filters;

/**
 * Class IsNullFilter
 * @package Nbo\RestApiBundle\Filters
 */
class IsNullFilter extends AbstractFilter {
    protected $sOperator = AbstractFilter::OPERATOR_IS_NULL;

    protected function build(): string
    {
        return $this->getPrefixedKey() . ' ' . $this->sOperator;
    }


}

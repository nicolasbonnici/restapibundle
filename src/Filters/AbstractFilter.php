<?php
namespace Nbo\RestApiBundle\Filters;

use Doctrine\ORM\QueryBuilder;

/**
 * Filters abstract layer
 *
 * Class AbstractFilter
 * @package Nbo\RestApiBundle\Filters
 */
class AbstractFilter
{
    const BOUNDED_PARAMETER_PREFIX = ':';

    /**
     * Query operators
     */
    const OPERATOR_EQUAL = '=';
    const OPERATOR_DIFFERENT = '!=';
    const OPERATOR_GREATER = '>';
    const OPERATOR_LESSER = '<';
    const OPERATOR_LIKE = 'LIKE';
    const OPERATOR_IS_NULL = 'IS NULL';
    const OPERATOR_IS_NOT_NULL = 'IS NOT NULL';

    /**
     * Supported query operators
     */
    const MAPPING = [
        self::OPERATOR_EQUAL => EqualFilter::class,
        self::OPERATOR_DIFFERENT => DifferentFilter::class,
        self::OPERATOR_GREATER => GreaterFilter::class,
        self::OPERATOR_LESSER => LesserFilter::class,
        self::OPERATOR_LIKE => LikeFilter::class,
        self::OPERATOR_IS_NULL => IsNullFilter::class,
        self::OPERATOR_IS_NOT_NULL => IsNotNullFilter::class
    ];

    /** @var string */
    protected $sPrefix = '';

    /** @var string */
    protected $sKey;

    /** @var mixed */
    protected $mValue;

    /** @var string  */
    protected $sOperator = self::OPERATOR_EQUAL;

    public function __construct(string $sKey, $mValue, $sPrefix = '')
    {
        $this->sKey = $sKey;
        $this->mValue = $mValue;
        $this->sPrefix = $sPrefix;

        $this->build();
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->build();
    }

    /**
     * Populate Doctrine Query with instance parameter
     *
     * @param QueryBuilder $oQuery
     * @return QueryBuilder
     */
    public function addQueryParameter(QueryBuilder $oQuery): QueryBuilder
    {
        return $oQuery->setParameter($this->getKey(), $this->getValue());
    }

    /**
     * Query expression build method
     * @return string
     */
    protected function build(): string
    {
        return $this->getPrefixedKey() . ' ' . $this->sOperator . ' ' . $this->prepareBoundedParameter();
    }

    protected function prepareBoundedParameter(): string
    {
        return self::BOUNDED_PARAMETER_PREFIX . $this->getKey();
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->sKey;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->mValue;
    }

    /**
     * @return string
     */
    public function getPrefix(): string
    {
        return (strlen($this->sPrefix) > 0)
            ? $this->sPrefix . '.'
            : '';
    }

    /**
     * @return string
     */
    public function getPrefixedKey(): string
    {
        return $this->getPrefix() . $this->getKey();
    }
}

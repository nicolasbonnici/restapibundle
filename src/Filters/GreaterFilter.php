<?php
namespace Nbo\RestApiBundle\Filters;

/**
 * Class GreaterFilter
 * @package Nbo\RestApiBundle\Filters
 */
class GreaterFilter extends AbstractFilter {
    protected $sOperator = AbstractFilter::OPERATOR_GREATER;
}

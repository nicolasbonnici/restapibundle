<?php

namespace Nbo\RestApiBundle\Tests\Entity;

use Nbo\RestApiBundle\Tests\Mock\Entity\SubResourceMock;
use PHPUnit\Framework\TestCase;
use Nbo\RestApiBundle\Tests\Mock\Entity\EntityMock;

/**
 * Business objects abstract layer tests
 *
 * Class EntityAbstractTest
 * @package RestApiBundle\Tests
 */
class AbstractResourceTest extends TestCase
{
    public function testResource()
    {
        $oMock = (new EntityMock())->setId(1)->setFoo('Bar');
        $sResourceName = $oMock->resource();
        self::assertIsString($sResourceName, 'Invalid AbstractResource::resource() out1put must be casted as an string.');
        self::assertEquals('mock', $sResourceName, 'Invalid AbstractResource::resource() output.');
    }

    public function testAttributes()
    {
        $oMock = (new EntityMock())->setId(1)->setFoo('Bar');
        $aAttrs = $oMock->attributes();
        self::assertIsArray($aAttrs, 'Invalid AbstractResource::getAttributes() output must be casted as an array.');
        self::assertEquals('id', $aAttrs[0], 'Invalid AbstractResource::getAttributes() output.');
        self::assertEquals('foo', $aAttrs[1], 'Invalid AbstractResource::getAttributes() output.');
        self::assertEquals('bar', $aAttrs[2], 'Invalid AbstractResource::getAttributes() output.');
    }

    public function testDetectSetterMethodName()
    {
        $oMock = (new EntityMock())->setFoo('Bar')->addSubEntity((new SubResourceMock())->setBar('foo'));
        $sDetectedSetter = $oMock->detectSetterMethodName('SubEntity');
        $this->assertEquals('addSubEntity', $sDetectedSetter, 'Wrong computation from AbstractEntity::detectSetterMethodName()');
    }

    public function testToArray()
    {
        $oMock = (new EntityMock())->setFoo('Bar');
        $aMock = $oMock->toArray();
        self::assertArrayHasKey('foo', $aMock, 'Key "foo" not found in AbstractResource::normalize() output.');
        self::assertIsArray($aMock, 'Invalid AbstractResource::normalize() output must be casted as an array.');
        self::assertArrayHasKey('id', $aMock, 'Invalid AbstractResource::toArray() output.');
        self::assertArrayHasKey('foo', $aMock, 'Invalid AbstractResource::toArray() output.');
        self::assertArrayHasKey('bar', $aMock, 'Invalid AbstractResource::toArray() output.');    }

    public function testToObject()
    {
        $oMock = (new EntityMock())->setFoo('Bar');
        $oMockedObject = $oMock->toObject();
        self::assertIsObject($oMockedObject, 'Invalid AbstractResource::toObject() output must be casted as an \stdClass object.');
        self::assertObjectHasAttribute('foo', $oMockedObject, 'Key "foo" not found in AbstractResource::toObject() output.');

        $oStdObject = new \stdClass();
        $oStdObject->id = null;
        $oStdObject->foo = 'Bar';
        $oStdObject->bar = null;

        self::assertObjectHasAttribute('id', $oMockedObject, 'Invalid AbstractResource::toObject() output.');;
        self::assertObjectHasAttribute('foo', $oMockedObject, 'Invalid AbstractResource::toObject() output.');
        self::assertObjectHasAttribute('bar', $oMockedObject, 'Invalid AbstractResource::toObject() output.');
    }

    public function testToString()
    {
        $oMock = (new EntityMock())->setFoo('Bar')->setId(33);
        self::assertEquals('Nbo\RestApiBundle\Tests\Mock\Entity\EntityMock#33', (string) $oMock, 'Invalid self::toString() output.');
    }
}

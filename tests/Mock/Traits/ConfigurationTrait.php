<?php
namespace Nbo\RestApiBundle\Tests\Mock\Traits;
trait ConfigurationTrait {
    public $configuration = [
        'mock' => [
            'classname' => 'Nbo\RestApiBundle\Tests\Mock\Entity\EntityMock',
            'fields_scope' => [],
            'methods' => [
                'GET' => [
                    'auth' => true,
                    'roles' => ['ROLE_TEST'],
                    'owner_restricted' => true
                ],
                'POST' => [
                    'auth' => true,
                    'roles' => ['ROLE_TEST'],
                    'owner_restricted' => true
                ],
                'PUT' => [
                    'auth' => true,
                    'roles' => ['ROLE_TEST'],
                    'owner_restricted' => true
                ],
                'DELETE' => [
                    'auth' => true,
                    'roles' => ['ROLE_TEST'],
                    'owner_restricted' => true
                ]
            ]
        ]
    ];
}

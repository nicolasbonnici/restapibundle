<?php
namespace Nbo\RestApiBundle\Tests\Mock\Entity;

use Nbo\RestApiBundle\Annotations\Resource;
use Nbo\RestApiBundle\Entity\AbstractResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @Resource(name="subresource")
 * @ORM\Entity()
 * @ORM\Table(name="submock")
 */
class SubResourceMock extends AbstractResource
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("public")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     * @Groups("public")
     */
    protected $foo;

    /**
     * @var string
     * @ORM\Column(type="string")
     * @Groups("private")
     */
    protected $bar;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return SubResourceMock
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getFoo(): ?string
    {
        return $this->foo;
    }

    /**
     * @param string $foo
     * @return SubResourceMock
     */
    public function setFoo($foo): self
    {
        $this->foo = $foo;
        return $this;
    }

    /**
     * @return string
     */
    public function getBar(): ?string
    {
        return $this->bar;
    }

    /**
     * @param string $bar
     * @return SubResourceMock
     */
    public function setBar(string $bar): self
    {
        $this->bar = $bar;
        return $this;
    }

}

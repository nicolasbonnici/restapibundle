<?php
namespace Nbo\RestApiBundle\Tests\Mock\Entity;

use Nbo\RestApiBundle\Annotations\Resource;
use Nbo\RestApiBundle\Entity\AbstractResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Nbo\RestApiBundle\Annotations\SubResource;

/**
 * @Resource(name="mock")
 * @ORM\Entity()
 * @ORM\Table(name="mock")
 */
class EntityMock extends AbstractResource
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("public")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     * @Groups("public")
     */
    protected $foo;

    /**
     * @var string
     * @ORM\Column(type="string")
     * @Groups("private")
     */
    protected $bar;

    /**
     * @SubResource(
     *     name="subentities",
     *     relation="ManyToMany",
     *     getter="getSubentities",
     *     setter="addSubEntity",
     *     remover="removeSubEntity"
     * )
     * @Groups("public")
     * @ORM\ManyToMany(targetEntity="Nbo\RestApiBundle\Tests\Mock\Entity\SubResourceMock", cascade={"persist"})
     */
    private $subEntities;

    /**
     * @SubResource(
     *     name="subresource",
     *     relation="OneToMany",
     *     getter="getSubResources",
     *     setter="addSubResource",
     *     remover="removeSubResource"
     * )
     * @Groups("public")
     * @ORM\OneToMany(targetEntity="Nbo\RestApiBundle\Tests\Mock\Entity\SubResourceMock", cascade={"persist"}, mappedBy="EntityMock")
     */
    private $subResources;

    /**
     * @SubResource(
     *     name="user",
     *     relation="OneToOne",
     *     getter="getUser",
     *     setter="setUser"
     * )
     * @Groups("public")
     * @ORM\OneToOne(targetEntity="Nbo\RestApiBundle\Tests\Mock\Entity\UserMock")
     */
    private $user;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     * @Groups("public")
     */
    protected $created;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     * @Groups("public")
     */
    protected $updated;

    public function __construct()
    {
        $this->subEntities = new ArrayCollection();
        $this->subResources = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return EntityMock
     */
    public function setId(int $id): EntityMock
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getFoo(): ?string
    {
        return $this->foo;
    }

    /**
     * @param string $foo
     * @return EntityMock
     */
    public function setFoo($foo): EntityMock
    {
        $this->foo = $foo;
        return $this;
    }

    /**
     * @return string
     */
    public function getBar(): ?string
    {
        return $this->bar;
    }

    /**
     * @param string $bar
     * @return EntityMock
     */
    public function setBar(string $bar): EntityMock
    {
        $this->bar = $bar;
        return $this;
    }

    /**
     * @return Collection|SubResourceMock[]
     */
    public function getSubEntities(): Collection
    {
        return $this->subEntities;
    }

    public function addSubEntity(SubResourceMock $contributor): self
    {
        if (!$this->subEntities->contains($contributor)) {
            $this->subEntities[] = $contributor;
        }

        return $this;
    }

    public function removeSubEntity(SubResourceMock $contributor): self
    {
        if ($this->subEntities->contains($contributor)) {
            $this->subEntities->removeElement($contributor);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubResources()
    {
        return $this->subResources;
    }

    public function addSubResource(SubResourceMock $contributor): self
    {
        if (!$this->subResources->contains($contributor)) {
            $this->subResources[] = $contributor;
        }

        return $this;
    }

    public function removeSubResource(SubResourceMock $contributor): self
    {
        if ($this->subResources->contains($contributor)) {
            $this->subResources->removeElement($contributor);
        }

        return $this;
    }

    /**
     * @return UserMock
     */
    public function getUser(): ?UserMock
    {
        return $this->user;
    }

    /**
     * @param UserMock $user
     * @return EntityMock
     */
    public function setUser(?UserMock $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): ?\DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     * @return EntityMock
     */
    public function setCreated(\DateTime $created): EntityMock
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated(): ?\DateTime
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     * @return EntityMock
     */
    public function setUpdated(\DateTime $updated): EntityMock
    {
        $this->updated = $updated;
        return $this;
    }

}

<?php
namespace Nbo\RestApiBundle\Tests\Filters;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Nbo\RestApiBundle\Filters\GreaterFilter;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class GreaterFilterTest extends KernelTestCase
{
    public function testQueryBuild()
    {
        $oGreaterFilter = new GreaterFilter('foo', 42);
        $this->assertEquals('foo > :foo', (string) $oGreaterFilter, 'GreaterFilter::__toString() invalid output');
    }

    public function testSetQueryParameter()
    {
        $oGreaterFilter = new GreaterFilter('foo', 42);
        $oQuery = new QueryBuilder($this->getManager());

        $oUpdatedQuery = $oGreaterFilter->addQueryParameter($oQuery);

        $this->assertEquals(42, $oUpdatedQuery->getParameter('foo')->getValue(), 'FilterAbstract::addQueryParameter() bad value for bounded parameter "foo".');
    }

    public function testParameterPrefix()
    {
        $oGreaterFilter = new GreaterFilter('foo', 'bar', 'x');

        $this->assertEquals('x.foo > :foo', (string) $oGreaterFilter, 'GreaterFilter::__toString() invalid output with prefix');
    }

    public function getManager(): EntityManagerInterface
    {
        self::bootKernel();
        $oContainer = self::$container;
        return $oContainer->get('doctrine.orm.default_entity_manager');
    }
}

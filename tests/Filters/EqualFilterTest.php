<?php
namespace Nbo\RestApiBundle\Tests\Filters;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Nbo\RestApiBundle\Filters\EqualFilter;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class EqualFilterTest extends KernelTestCase
{
    public function testQueryBuild()
    {
        $oEqualFilter = new EqualFilter('foo', 'bar');
        $this->assertEquals('foo = :foo', (string) $oEqualFilter, 'EqualFilter::__toString() invalid output');
    }

    public function testSetQueryParameter()
    {
        $oEqualFilter = new EqualFilter('foo', 'bar');
        $oQuery = new QueryBuilder($this->getManager());

        $oUpdatedQuery = $oEqualFilter->addQueryParameter($oQuery);

        $this->assertEquals('bar', $oUpdatedQuery->getParameter('foo')->getValue(), 'FilterAbstract::addQueryParameter() bad value for bounded parameter "foo".');
    }

    public function testParameterPrefix()
    {
        $oEqualFilter = new EqualFilter('foo', 'bar', 'x');

        $this->assertEquals('x.foo = :foo', (string) $oEqualFilter, 'EqualFilter::__toString() invalid output with prefix');
    }

    public function getManager(): EntityManagerInterface
    {
        self::bootKernel();
        $oContainer = self::$container;
        return $oContainer->get('doctrine.orm.default_entity_manager');
    }
}

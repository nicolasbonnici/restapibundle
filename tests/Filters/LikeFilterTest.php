<?php
namespace Nbo\RestApiBundle\Tests\Filters;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Nbo\RestApiBundle\Filters\LikeFilter;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class LikeFilterTest extends KernelTestCase
{
    public function testQueryBuild()
    {
        $oEqualFilter = new LikeFilter('foo', 'bar');
        $this->assertEquals('foo LIKE :foo', (string) $oEqualFilter, 'LikeFilter::__toString() invalid output');
    }

    public function testSetQueryParameterDefaultBehavior()
    {
        $oEqualFilter = new LikeFilter('foo', 'bar');
        $oQuery = new QueryBuilder($this->getManager());

        $oUpdatedQuery = $oEqualFilter->addQueryParameter($oQuery);

        $this->assertEquals('%bar%', $oUpdatedQuery->getParameter('foo')->getValue(), 'LikeFilter::addQueryParameter() bad value for bounded parameter "foo".');
    }

    public function testSetQueryParameterWithStartWildcardBehavior()
    {
        $oEqualFilter = new LikeFilter('foo', 'bar');
        $oQuery = new QueryBuilder($this->getManager());

        $oUpdatedQuery = $oEqualFilter->addQueryParameter($oQuery, LikeFilter::WILDCARD_START);

        $this->assertEquals('%bar', $oUpdatedQuery->getParameter('foo')->getValue(), 'LikeFilter::addQueryParameter() bad value for bounded parameter "foo".');
    }

    public function testSetQueryParameterWithEndWildcardBehavior()
    {
        $oEqualFilter = new LikeFilter('foo', 'bar');
        $oQuery = new QueryBuilder($this->getManager());

        $oUpdatedQuery = $oEqualFilter->addQueryParameter($oQuery, LikeFilter::WILDCARD_END);

        $this->assertEquals('bar%', $oUpdatedQuery->getParameter('foo')->getValue(), 'LikeFilter::addQueryParameter() bad value for bounded parameter "foo".');
    }

    public function getManager(): EntityManagerInterface
    {
        self::bootKernel();
        $oContainer = self::$container;
        return $oContainer->get('doctrine.orm.default_entity_manager');
    }
}

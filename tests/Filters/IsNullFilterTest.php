<?php
namespace Nbo\RestApiBundle\Tests\Filters;

use Doctrine\ORM\EntityManagerInterface;
use Nbo\RestApiBundle\Filters\IsNullFilter;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class IsNullFilterTest extends KernelTestCase
{
    public function testQueryBuild()
    {
        $oIsNullFilter = new IsNullFilter('foo', null);
        $this->assertEquals('foo IS NULL', (string) $oIsNullFilter, 'IsNullFilter::__toString() invalid output');
    }

    public function testParameterPrefix()
    {
        $oIsNullFilter = new IsNullFilter('foo', null, 'x');

        $this->assertEquals('x.foo IS NULL', (string) $oIsNullFilter, 'IsNullFilter::__toString() invalid output with prefix');
    }

    public function getManager(): EntityManagerInterface
    {
        self::bootKernel();
        $oContainer = self::$container;
        return $oContainer->get('doctrine.orm.default_entity_manager');
    }
}

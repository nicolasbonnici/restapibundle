<?php
namespace Nbo\RestApiBundle\Tests\Filters;

use Doctrine\ORM\EntityManagerInterface;
use Nbo\RestApiBundle\Filters\IsNotNullFilter;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class IsNotNullFilterTest extends KernelTestCase
{
    public function testQueryBuild()
    {
        $oIsNotNullFilter = new IsNotNullFilter('foo', null);
        $this->assertEquals('foo IS NOT NULL', (string) $oIsNotNullFilter, 'IsNotNullFilter::__toString() invalid output');
    }

    public function testParameterPrefix()
    {
        $oIsNotNullFilter = new IsNotNullFilter('foo', null, 'x');

        $this->assertEquals('x.foo IS NOT NULL', (string) $oIsNotNullFilter, 'IsNotNullFilter::__toString() invalid output with prefix');
    }

    public function getManager(): EntityManagerInterface
    {
        self::bootKernel();
        $oContainer = self::$container;
        return $oContainer->get('doctrine.orm.default_entity_manager');
    }
}

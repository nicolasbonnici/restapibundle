<?php
namespace Nbo\RestApiBundle\Tests\Filters;

use Doctrine\ORM\QueryBuilder;
use Nbo\RestApiBundle\Filters\DifferentFilter;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class DifferentFilterTest extends KernelTestCase
{
    private $oEntityManager;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->oEntityManager = static::$kernel
            ->getContainer()
            ->get('doctrine.orm.default_entity_manager');
    }

    public function testQueryBuild()
    {
        $oDifferentFilter = new DifferentFilter('foo', 'bar');
        $this->assertEquals('foo != :foo', (string) $oDifferentFilter, 'DifferentFilter::__toString() invalid output');
    }

    public function testSetQueryParameter()
    {
        $oDifferentFilter = new DifferentFilter('foo', 'bar');
        $oQuery = new QueryBuilder($this->oEntityManager);

        $oUpdatedQuery = $oDifferentFilter->addQueryParameter($oQuery);

        $this->assertEquals('bar', $oUpdatedQuery->getParameter('foo')->getValue(), 'FilterAbstract::addQueryParameter() bad value for bounded parameter "foo".');
    }

    public function testParameterPrefix()
    {
        $oDifferentFilter = new DifferentFilter('foo', 'bar', 'x');

        $this->assertEquals('x.foo != :foo', (string) $oDifferentFilter, 'DifferentFilter::__toString() invalid output with prefix');
    }

}

<?php
namespace Nbo\RestApiBundle\Tests\Filters;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Nbo\RestApiBundle\Filters\LesserFilter;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class LesserFilterTest extends KernelTestCase
{
    public function testQueryBuild()
    {
        $oEqualFilter = new LesserFilter('foo', 42);
        $this->assertEquals('foo < :foo', (string) $oEqualFilter, 'LesserFilter::__toString() invalid output');
    }

    public function testSetQueryParameter()
    {
        $oEqualFilter = new LesserFilter('foo', 42);
        $oQuery = new QueryBuilder($this->getManager());

        $oUpdatedQuery = $oEqualFilter->addQueryParameter($oQuery);

        $this->assertEquals(42, $oUpdatedQuery->getParameter('foo')->getValue(), 'FilterAbstract::addQueryParameter() bad value for bounded parameter "foo".');
    }

    public function testParameterPrefix()
    {
        $oGreaterFilter = new LesserFilter('foo', 'bar', 'x');

        $this->assertEquals('x.foo < :foo', (string) $oGreaterFilter, 'LesserFilter::__toString() invalid output with prefix');
    }

    public function getManager(): EntityManagerInterface
    {
        self::bootKernel();
        $oContainer = self::$container;
        return $oContainer->get('doctrine.orm.default_entity_manager');
    }
}

<?php

namespace Nbo\RestApiBundle\Tests\Serializer;

use Doctrine\Common\Collections\ArrayCollection;
use Nbo\RestApiBundle\Serializer\ResourcesSerializer;
use Nbo\RestApiBundle\Tests\Mock\Entity\EntityMock;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

class ResourcesSerializerTest extends KernelTestCase
{
    public function testNormalizationWithEntityInput()
    {
        $oResourcesSerializer = $this->getResourcesSerializer();

        $oDateTime = new \DateTime('now');
        $oMock = (new EntityMock())->setId(42)->setFoo('bar')->setBar('foo')->setCreated($oDateTime)->setUpdated($oDateTime);

        $aNormalized = $oResourcesSerializer->normalize($oMock);

        $this->assertArrayHasKey('id', $aNormalized, 'Missing id field from ResourcesSerializer');
        $this->assertEquals(42, $aNormalized['id'], 'Wrong value for id returned by ResourcesSerializer');
        $this->assertArrayHasKey('foo', $aNormalized, 'Missing foo field from ResourcesSerializer');
        $this->assertEquals('bar', $aNormalized['foo'], 'Wrong value for foo returned by ResourcesSerializer');
        $this->assertArrayHasKey('bar', $aNormalized, 'Missing bar field from ResourcesSerializer');
        $this->assertEquals('foo', $aNormalized['bar'], 'Wrong value for bar returned by ResourcesSerializer');
        $this->assertArrayHasKey('created', $aNormalized, 'Missing created field from ResourcesSerializer');
        $this->assertTrue(is_string($aNormalized['created']), 'Created field not serialized from ResourcesSerializer');
        $this->assertEquals($oDateTime->format(DATE_ATOM), $aNormalized['created'], 'Created field not correctly formatted expecting ATOM format.');
    }

    public function testNormalizationWithArrayInput()
    {
        $oResourcesSerializer = $this->getResourcesSerializer();
        $oDateTime = new \DateTime('now');
        $aEntities = [
            (new EntityMock())->setId(42)->setFoo('bar')->setBar('foo')->setCreated($oDateTime)->setUpdated($oDateTime),
            (new EntityMock())->setId(43)->setFoo('bar')->setBar('foo')->setCreated($oDateTime)->setUpdated($oDateTime),
            (new EntityMock())->setId(44)->setFoo('bar')->setBar('foo')->setCreated($oDateTime)->setUpdated($oDateTime)
        ];

        $aNormalizedEntities = $oResourcesSerializer->normalize($aEntities);
        foreach ($aNormalizedEntities as $iIndex => $aSerialized) {
            $this->assertArrayHasKey('id', $aSerialized, 'Missing id field from ResourcesSerializer with array input');
            $this->assertEquals(42 + $iIndex, $aSerialized['id'], 'Wrong value for id returned by ResourcesSerializer with array input');
            $this->assertArrayHasKey('foo', $aSerialized, 'Missing foo field from ResourcesSerializer with array input');
            $this->assertEquals('bar', $aSerialized['foo'], 'Wrong value for foo returned by ResourcesSerializer with array input');
            $this->assertArrayHasKey('bar', $aSerialized, 'Missing bar field from ResourcesSerializer with array input');
            $this->assertEquals('foo', $aSerialized['bar'], 'Wrong value for bar returned by ResourcesSerializer with array input');
            $this->assertArrayHasKey('created', $aSerialized, 'Missing created field from ResourcesSerializer');
            $this->assertTrue(is_string($aSerialized['created']), 'Created field not serialized from ResourcesSerializer');
            $this->assertEquals($oDateTime->format(DATE_ATOM), $aSerialized['created'], 'Created field not correctly formatted expecting ATOM format.');
        }

    }

    public function testNormalizationWithArrayCollectionInput()
    {
        $oResourcesSerializer = $this->getResourcesSerializer();
        $oDateTime = new \DateTime('now');
        $aEntities = new ArrayCollection([
            (new EntityMock())->setId(42)->setFoo('bar')->setBar('foo')->setCreated($oDateTime)->setUpdated($oDateTime),
            (new EntityMock())->setId(43)->setFoo('bar')->setBar('foo')->setCreated($oDateTime)->setUpdated($oDateTime),
            (new EntityMock())->setId(44)->setFoo('bar')->setBar('foo')->setCreated($oDateTime)->setUpdated($oDateTime)
        ]);

        $aNormalizedEntities = $oResourcesSerializer->normalize($aEntities);

        foreach ($aNormalizedEntities as $iIndex => $aSerialized) {
            $this->assertArrayHasKey('id', $aSerialized, 'Missing id field from ResourcesSerializer with array input');
            $this->assertEquals(42 + $iIndex, $aSerialized['id'], 'Wrong value for id returned by ResourcesSerializer with array input');
            $this->assertArrayHasKey('foo', $aSerialized, 'Missing foo field from ResourcesSerializer with array input');
            $this->assertEquals('bar', $aSerialized['foo'], 'Wrong value for foo returned by ResourcesSerializer with array input');
            $this->assertArrayHasKey('bar', $aSerialized, 'Missing bar field from ResourcesSerializer with array input');
            $this->assertEquals('foo', $aSerialized['bar'], 'Wrong value for bar returned by ResourcesSerializer with array input');
            $this->assertArrayHasKey('created', $aSerialized, 'Missing created field from ResourcesSerializer');
            $this->assertTrue(is_string($aSerialized['created']), 'Created field not serialized from ResourcesSerializer');
            $this->assertEquals($oDateTime->format(DATE_ATOM), $aSerialized['created'], 'Created field not correctly formatted expecting ATOM format.');
        }
    }

    public function testNormalizationWithGroupAnnotationScope()
    {
        $oResourcesSerializer = $this->getResourcesSerializer();
        $oDateTime = new \DateTime('now');
        $oMock = (new EntityMock())->setId(42)->setFoo('bar')->setBar('foo')->setCreated($oDateTime)->setUpdated($oDateTime);

        $aNormalized = $oResourcesSerializer->normalize($oMock, 'array', [AbstractNormalizer::GROUPS => 'public']);

        $this->assertFalse(array_key_exists('bar', $aNormalized), 'Groups annotation ignored.');
    }

    public function testEntitySerialization()
    {
        $oResourcesSerializer = $this->getResourcesSerializer();
        $oDateTime = new \DateTime('now');
        $oMock = (new EntityMock())->setId(42)->setFoo('bar')->setBar('foo')->setCreated($oDateTime)->setUpdated($oDateTime);

        $sSerialized = $oResourcesSerializer->serialize($oMock, 'json');

        $this->assertIsString($sSerialized, 'Wrong type returned by serializer, must be string.');
        $this->assertObjectHasAttribute('id', json_decode($sSerialized), 'Unable to retrieve id after serialization');
    }

    private function getResourcesSerializer(): ResourcesSerializer
    {
        self::bootKernel();
        $oContainer = self::$container;
        return $oContainer->get('Nbo\RestApiBundle\Serializer\ResourcesSerializer');
    }

}

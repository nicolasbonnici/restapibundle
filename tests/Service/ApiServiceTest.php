<?php

namespace Nbo\RestApiBundle\Tests\Service;

use Nbo\RestApiBundle\Serializer\ResourcesSerializer;
use Nbo\RestApiBundle\Service\ApiService;
use Nbo\RestApiBundle\Tests\Mock\Entity\UserMock;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Request;

class ApiServiceTest extends KernelTestCase
{

    public function testBuildRoot()
    {
        $oRequest = new Request();
        $oRequest->setLocale('fr');
        $oUser = (new UserMock())->setId(99)
            ->setEmail('user@test.com')
            ->setDisplayName('PHPunit user')
            ->setRoles(['ROLE_TERMINATOR'])
            ->setCreatedAt(new \DateTime('now'));

        $aResponse = $this->getApiService()->buildRoot(
            self::$container,
            $oUser,
            new ResourcesSerializer()
        );

        self::assertTrue(is_array($aResponse), 'Wrong return type from ApiService::buildRoot()');
        self::assertArrayHasKey('version', $aResponse, 'Missing "version" key from ApiService::buildRoot() response.');
        self::assertArrayHasKey('api_root', $aResponse, 'Missing "api_root" key from ApiService::buildRoot() response.');
        self::assertArrayHasKey('configuration', $aResponse, 'Missing "configuration" key from ApiService::buildRoot() response.');
        self::assertArrayHasKey('composer', $aResponse, 'Missing "composer" key from ApiService::buildRoot() response.');
        self::assertArrayHasKey('authenticated_user', $aResponse, 'Missing "authenticated_user" key from ApiService::buildRoot() response.');
        self::assertArrayHasKey('id', $aResponse['authenticated_user'], 'Missing "id" key from ApiService::buildRoot() authenticated user response.');
        self::assertArrayHasKey('displayName', $aResponse['authenticated_user'], 'Missing "displayName" key from ApiService::buildRoot() authenticated user response.');
        self::assertEquals(99, $aResponse['authenticated_user']['id'], 'Wrong value for "id" field on ApiService::buildRoot() returned user.');
        self::assertEquals('PHPunit user', $aResponse['authenticated_user']['displayName'], 'Wrong value for "displayName" field on ApiService::buildRoot() returned user.');

        // Check for sub resources detection
        self::assertEquals(ApiService::SUB_RESOURCE_TYPE_PREFIX . 'user', $aResponse['configuration']['resources']['mock']['model']['user'], 'Bad type for sub resource with 1 to 1 or many to 1 relations.');
        self::assertEquals(ApiService::SUB_RESOURCES_TYPE_PREFIX . 'subresource', $aResponse['configuration']['resources']['mock']['model']['subResources'], 'Bad type for *Many sub resources.');
        self::assertEquals(ApiService::SUB_RESOURCES_TYPE_PREFIX . 'subentities', $aResponse['configuration']['resources']['mock']['model']['subEntities'], 'Bad type for *Many sub entities.');
    }

    private function getApiService()
    {
        self::bootKernel();
        $oContainer = self::$container;
        return $oContainer->get('Nbo\RestApiBundle\Service\ApiService');
    }

}

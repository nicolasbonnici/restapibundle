<?php

namespace Nbo\RestApiBundle\Tests\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\QueryBuilder;
use Nbo\RestApiBundle\Service\CrudService;
use Nbo\RestApiBundle\Tests\Mock\Entity\EntityMock;
use Nbo\RestApiBundle\Tests\Mock\Entity\SubResourceMock;
use Nbo\RestApiBundle\Tests\Mock\Entity\UserMock;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CrudServiceTest extends KernelTestCase
{

    public function testPopulate()
    {
        $aInput = ['foo' => 'bar', 'bar' => 'foo'];
        $oMock = new EntityMock();

        $this->getCrudService()->populate($oMock, $aInput);

        $this->assertEquals('bar', $oMock->getFoo(), 'AbstractEntity::populate() error.');
        $this->assertEquals('foo', $oMock->getBar(), 'AbstractEntity::populate() error.');
    }

    public function testPopulateWithOneToOneRelation()
    {
        $oUser = (new UserMock())->setEmail('user@test.com');

        $aInput = ['foo' => 'bar', 'bar' => 'foo', 'user' => ["email" => "user@test.com"]];
        $oMock = new EntityMock();

        $this->getCrudService()->populate($oMock, $aInput);

        $this->assertEquals('bar', $oMock->getFoo(), 'AbstractEntity::populate() error.');
        $this->assertEquals('foo', $oMock->getBar(), 'AbstractEntity::populate() error.');
        $this->assertEquals($oUser, $oMock->getUser(), 'AbstractEntity::populate() error with OneToOne relation.');
    }

    public function testPopulateWithOneOrManyToManyRelations()
    {
        $oUser = (new UserMock())->setEmail('user@test.com');

        $aInput = [
            'foo' => 'bar',
            'bar' => 'foo',
            'user' => ['email' => 'user@test.com'],
            'subResources' => [['foo' => 'bar'],['foo' => 'bar'],['foo' => 'bar'],['foo' => 'bar'],['foo' => 'bar'],['foo' => 'bar']],
            'subEntities' => [['foo' => 'bar'],['foo' => 'bar'],['foo' => 'bar'],['foo' => 'bar'],['foo' => 'bar'],['foo' => 'bar']],
        ];
        $oMock = new EntityMock();

        $this->getCrudService()->populate($oMock, $aInput);

        $this->assertEquals('bar', $oMock->getFoo(), 'AbstractEntity::populate() error.');
        $this->assertEquals('foo', $oMock->getBar(), 'AbstractEntity::populate() error.');
        $this->assertEquals($oUser, $oMock->getUser(), 'AbstractEntity::populate() error with OneToOne relation.');
        $this->assertIsObject($oMock->getSubResources(), 'AbstractEntity::populate() wrong type with OneToMany relation.');
        $this->assertTrue($oMock->getSubResources() instanceof ArrayCollection, 'AbstractEntity::populate() error with OneToMany relation.');
        $this->assertTrue($oMock->getSubResources()->first() instanceof SubResourceMock, 'AbstractEntity::populate() wrong type with OneToMany relation.');
        $this->assertEquals('bar', $oMock->getSubResources()->first()->getFoo(), 'AbstractEntity::populate() wrong value with OneToMany relation.');
        $this->assertIsObject($oMock->getSubEntities(), 'AbstractEntity::populate() wrong type with ManyToMany relation.');
        $this->assertTrue($oMock->getSubEntities() instanceof ArrayCollection, 'AbstractEntity::populate() error with ManyToMany relation.');
        $this->assertTrue($oMock->getSubEntities()->first() instanceof SubResourceMock, 'AbstractEntity::populate() wrong type with ManyToMany relation.');
        $this->assertEquals('bar', $oMock->getSubEntities()->first()->getFoo(), 'AbstractEntity::populate() wrong value with ManyToMany relation.');
    }

    public function testBuildQuery()
    {
        /** @var \ReflectionClass $oReflectedService */
        $oReflectedService = new \ReflectionClass(CrudService::class);
        $oQueryMethod = $oReflectedService->getMethod('buildQuery');
        $oQueryMethod->setAccessible(true);

        /** @var QueryBuilder $oQuery */
        $oQuery = $oQueryMethod->invoke(
            $this->getCrudService(),
            EntityMock::class,
            ['foo' => 'bar'],
            'en',
            ['created'=>'DESC'],
            1,
            42
        );

        $this->assertTrue($oQuery instanceof QueryBuilder);
        $this->assertEquals(42, $oQuery->getMaxResults(), 'Limit parameter not setted on ouput QueryBuilder');
        $this->assertEquals('bar', $oQuery->getParameter('foo')->getValue(), 'Parameter foo" not added on buildQuery WHERE clause');
        $this->assertEquals(
            'SELECT m0_.id AS id_0, m0_.foo AS foo_1, m0_.bar AS bar_2, m0_.created AS created_3, m0_.updated AS updated_4, m0_.user_id AS user_id_5 FROM mock m0_ WHERE m0_.foo = ? ORDER BY m0_.created DESC LIMIT 42',
            $oQuery->getQuery()->getSQL(),
            'Invalid CrudService::buildQuery() QueryBuilder SQL output'
        );
    }

    public function testBuildQueryWithLikeFilter()
    {
        /** @var \ReflectionClass $oReflectedService */
        $oReflectedService = new \ReflectionClass(CrudService::class);
        $oQueryMethod = $oReflectedService->getMethod('buildQuery');
        $oQueryMethod->setAccessible(true);

        /** @var QueryBuilder $oQuery */
        $oQuery = $oQueryMethod->invoke(
            $this->getCrudService(),
            EntityMock::class,
            ['foo' => ['LIKE' => 'bar']],
            'en',
            ['created'=>'DESC'],
            1,
            42
        );

        $this->assertTrue($oQuery instanceof QueryBuilder);
        $this->assertEquals(42, $oQuery->getMaxResults(), 'Limit parameter not setted on ouput QueryBuilder');
        $this->assertEquals('%bar%', $oQuery->getParameter('foo')->getValue(), 'Parameter foo" not added on buildQuery WHERE clause');
        $this->assertEquals(
            'SELECT m0_.id AS id_0, m0_.foo AS foo_1, m0_.bar AS bar_2, m0_.created AS created_3, m0_.updated AS updated_4, m0_.user_id AS user_id_5 FROM mock m0_ WHERE m0_.foo LIKE ? ORDER BY m0_.created DESC LIMIT 42',
            $oQuery->getQuery()->getSQL(),
            'Invalid CrudService::buildQuery() QueryBuilder SQL output'
        );
    }

    private function getCrudService()
    {
        self::bootKernel();
        $oContainer = self::$container;
        return $oContainer->get('Nbo\RestApiBundle\Service\CrudService');
    }

}

<?php

namespace Nbo\RestApiBundle\Tests\Service;

use Nbo\RestApiBundle\Service\AclService;
use Nbo\RestApiBundle\Tests\Mock\Entity\EntityMock;
use Nbo\RestApiBundle\Tests\Mock\Entity\UserMock;
use Nbo\RestApiBundle\Tests\Mock\Traits\ConfigurationTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\Container;

class AclServiceTest extends KernelTestCase
{
    use ConfigurationTrait;

    const RESOURCE_NAME = 'mock';
    const ENTITY_CLASSNAME = 'Nbo\RestApiBundle\Tests\Mock\Entity\EntityMock';

    public function testCheckAuthRequiredWithoutUser()
    {
        try {
            $sUnauthorizedHttpException = 'Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException';
            $oAclService = $this->getAclService();

            self::assertInstanceOf(
                $sUnauthorizedHttpException,
                $oAclService->check(null, (new EntityMock()), 'get'),
                'Must throw HTTP unauthorized exception.'
            );
            self::expectException($sUnauthorizedHttpException);
            self::expectExceptionCode(401);
        } catch (\Exception $oException) {
            self::assertEquals(
                $sUnauthorizedHttpException,
                get_class($oException),
                __METHOD__ . ': Invalid exception type returned.'
            );
            self::assertTrue(method_exists($oException, 'getstatusCode'), __METHOD__ . ': No status code returned.');
            self::assertEquals(401, $oException->getStatusCode(), __METHOD__ . ': Invalid exception status code returned.');
        }
    }

    public function testCheckWithoutRequiredRole()
    {
        $sUnauthorizedHttpException = 'Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException';

        $oAclService = $this->getAclService();

        try {
            $oAclService->check((new UserMock())->setEmail('foo@bar.tld')->setRoles(['ROLE_USER']), (new EntityMock()), 'get');
            self::expectException($sUnauthorizedHttpException);
            self::expectExceptionCode(401);
        } catch (\Exception $oException) {
            self::assertEquals(
                $sUnauthorizedHttpException,
                get_class($oException),
                __METHOD__ . ': Invalid exception type returned.'
            );
            self::assertTrue(method_exists($oException, 'getStatusCode'), __METHOD__ . ': No status code returned.');
            self::assertEquals(401, $oException->getStatusCode(), __METHOD__ . ': Invalid exception status code returned.');        }

    }

    public function testCheckWithRequiredRoleReturnTrue()
    {
        $oAclService = $this->getAclService();

        $this->assertTrue(
            $oAclService->check((new UserMock())->setEmail('foo@bar.tld')->setRoles(['ROLE_TERMINATOR']), (new EntityMock()), 'get'),
            'Wrong return from AclService, request must pass.'
        );
    }

    public function testCheckWithAdminRoleAlwaysReturnTrue()
    {
        $oAclService = $this->getAclService();

        $this->assertTrue(
            $oAclService->check((new UserMock())->setEmail('foo@bar.tld')->setRoles(['ROLE_ADMIN']), (new EntityMock()), 'get'),
            'Wrong return from AclService, ROLE_ADMIN must pass.'
        );
    }

    private function getAclService(): AclService
    {
        self::bootKernel();
        /** @var Container $oContainer */
        $oContainer = self::$container;
        return $oContainer->get('Nbo\RestApiBundle\Service\AclService');
    }

}
